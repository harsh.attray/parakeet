import { ParakeetBotPage } from './app.po';

describe('parakeet-bot App', function() {
  let page: ParakeetBotPage;

  beforeEach(() => {
    page = new ParakeetBotPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
