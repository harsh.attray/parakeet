import { BrowserModule } from '@angular/platform-browser';
import { NgModule,Injectable,OnInit } from '@angular/core';
import { Routes,RouterModule,ActivatedRoute } from '@angular/router';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import { FormsModule,
    ReactiveFormsModule,
    FormBuilder,
    FormGroup,FormControl,Validators,AbstractControl } from '@angular/forms';
import { Http,Response,RequestOptions,Headers,HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { TweetComponent } from './tweet.component';
import { YsearchComponent,YsearchService,SearchBoxComponent,SearchResultsComponent } from './ysearch.component';
import { HomeComponent,AboutComponent } from './routes/track.components';
import {yServiceInjectables} from './ysearch.component';
import { TweetListComponent } from './tweetlist.component';
import { MusicSearchComponent,ArtistComponent,TracksComponent,AlbumComponent } from './music.component';

const routes : Routes = [
    {path:'',redirectTo:'home',pathMatch:'full'},
    { path:'home',component: HomeComponent },
    { path:'about',component: AboutComponent },
    { path:'Y-search',component: YsearchComponent },
    { path:'tweet', component: TweetListComponent },
    { path:'music/:id', component: MusicSearchComponent},
    { path:'artists/:id', component: ArtistComponent},
    { path:'tracks/:id', component: TracksComponent},
    { path:'albums/:id', component: AlbumComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    TweetComponent,
    YsearchComponent,
    SearchBoxComponent,
    SearchResultsComponent,
    TweetListComponent,
    MusicSearchComponent,
    HomeComponent,
    AboutComponent,
    ArtistComponent,
    TracksComponent,
    AlbumComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      RouterModule.forRoot(routes)
  ],
  providers: [yServiceInjectables,YsearchService,{ provide: LocationStrategy, useClass: PathLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule {}
