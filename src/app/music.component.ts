import { Component } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router'

//Music Search Component
@Component({
    selector: 'music-search',
    templateUrl :'./music.component.html',
    styleUrls:[]
})

export class MusicSearchComponent{
    id: string;
    constructor( private route:ActivatedRoute){
      route.params.subscribe(params=>{
          this.id = params['id'];
      });
    }
}

//Artist Component

@Component({
    selector:'artist-section',
    template:``
})
export class ArtistComponent{
    id:string;
    constructor(private route:ActivatedRoute){
        route.params.subscribe(params=>{
          this.id = params['id'];
        })
    }
}
//AlbumComponent

@Component({
    selector : 'album-section',
    template:``
})

export class AlbumComponent{
    id:string;
    constructor( private route: ActivatedRoute){
        route.params.subscribe(params=>{
            this.id = params['id'];
        })
    }
}

//Tracks Component
@Component({
    selector:'tracks-section',
    template:``
})
export class TracksComponent{
    id:string;
    constructor( private route:ActivatedRoute){
        route.params.subscribe(params=>{
            this.id = params['id'];
        })
    }
}
