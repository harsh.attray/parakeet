import { Component } from '@angular/core';
import { Http, HttpModule, RequestOptions,Response,Headers } from '@angular/http';

@Component({
    selector: 'post-request',
    templateUrl:'./post-request.html',
    styleUrls:[]
})

//POST Request
export class PostRequestComponent {
    data : Object;
    loading :boolean;
    constructor(private http: Http){
          this.http.post('http://jsonplaceholder.typicode.com/posts',JSON.stringify({
              body:'bar',
              title:'abc',
              userId: 1
          })).subscribe((res:Response)=>{
              this.data = res.json();
              this.loading = false;
          });
    }
}
//DELETE Request

export class DeleteRequestComponent{
    data :Object;
    loading : boolean;
    constructor(private http: Http){
        this.http.delete('http://someshitURL.io').subscribe((res:Response)=>{
             this.data= res.json();
             this.loading = false;
        })
    }
}
//PATCH Request
export class PatchRequestComponent{
    data: Object;
    loading :boolean;
    constructor(private http: Http){
        this.http.patch('http://asswipe.io',JSON.stringify({
            body:'bar',
            title:'abc',
            userId:1
        })).subscribe((res:Response)=>{
            this.data = res.json();
            this.loading = false;
        })
    }
}

//Request Options 
makeHeaders=():void=>{
    let headers: Headers = new Headers();
    headers.append('X-API-TOKEN','abcd');
    let opts:RequestOptions = new RequestOptions();
    opts.headers = headers;

    this.http.get('http://testwtf.io',opts).subscribe((res:Response)=>{
        this.data = res.json();
    })
}
