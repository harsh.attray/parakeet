import { Component, Input, OnInit } from '@angular/core';
import { Tweet } from './tweet.model';


@Component({
    selector : 'tweet-component',
    host:{
        class : 'row'
    },
    templateUrl : './tweet.component.html'
})
export class TweetComponent{
    @Input() tweet :Tweet;

    constructor(){
    }
     voteUp():boolean{
        this.tweet.voteUp();
        return false;
     }

     voteDown():boolean{
         this.tweet.voteDown();
        return false;

     }
 }
