export class Tweet{
    title : string;
    period : number;
    votes: number;
    constructor(title:string,period:number,votes ?:number){
      this.title = title;
      this.period = period;
      this.votes = votes ||0;
    }
    voteUp():void{
       this.votes +=1;
    }
    voteDown():void{
        if(this.votes >0){
            this.votes -=1;
        }
        else{
            this.votes = 0;
        }
    }
}
