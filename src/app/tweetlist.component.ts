import { Component } from '@angular/core';
import {HttpModule,Response,Http,RequestOptions,Headers } from '@angular/http';
import{ FormBuilder,FormGroup,FormControl,Validators,AbstractControl,ReactiveFormsModule} from '@angular/forms';
import { Tweet } from './tweet.model';


export var consumer_key:string =  '';
export var consumer_secret:string = '';
export var access_token_secret :string ="";
export var access_token : string ="";

declare function require(name:string);
var Twit = require('Twit');

@Component({
    selector:'tweet-list',
    templateUrl: './tweetlist.component.html'
})

export class TweetListComponent {
    tweets : Tweet[];
    myForm : FormGroup;
    search : AbstractControl;
    period : AbstractControl;
    params : Object;
    //model declaration
    query_name : string;
    time_period :string;
    data: Object;

    constructor(fb:FormBuilder){
        this.myForm = fb.group({
            'search': [null,Validators.compose([Validators.required,searchValidator])],
            'period' : [null,Validators.compose([Validators.required,periodValidator])]
        })
        this.search = this.myForm.controls['search'];
        this.period = this.myForm.controls['period'];
        this.tweets = [];
        this.params = {
            q: 'twitter since:2011-07-11',
            count: 2
        };

        //Observers added to field
        this.search.valueChanges.subscribe((value:string)=>{
        });
        this.period.valueChanges.subscribe((value:string)=>{
        });
        //Observers added to form
        this.myForm.valueChanges.subscribe((value:any)=>{
            console.log(this.params);
        });
        this.stalk= new Twit({consumer_key,consumer_secret,access_token_secret,access_token});
    this.stalk('search/tweets',this.params,function(response:Response,data:Object):void{
        console.log(this.data);
        });
    }
  getTweets=(value:any):boolean =>{
      this.tweets.push(new Tweet(value.search,value.period));
      return false;
  }
  sortedTweets=():Tweet[] =>{
      return this.tweets.sort((a:Tweet, b:Tweet)=>b.votes-a.votes);
  }
}
function searchValidator(control: FormControl): { [s: string]: boolean } {
  if (control.value != null && !control.value.match(/^abc/)) {
    return {invalidSearch: true};
  }
}
function periodValidator(control: FormControl): { [s: string]: boolean } {
  if (control.value != null && !control.value.match(/^123/)) {
    return {invalidPeriod: true};
  }
}
