import { Component,Injectable,Inject,EventEmitter,OnInit,ElementRef } from '@angular/core';
import { Http,Response, RequestOptions,Headers,HttpModule } from '@angular/http';
import { Observable } from 'rxjs';

export var Y_API_KEY :string ="AIzaSyBsyZ6qHMCjwI8erSKwu1jeBTKNgwHjE0g";
export var Y_API_URL :string = "https://www.googleapis.com/youtube/v3/search";

class SearchResult {
  id: string;
  title: string;
  description: string;
  thumbnailUrl: string;
  videoUrl: string;

  constructor(obj?: any) {
    this.id              = obj && obj.id             || null;
    this.title           = obj && obj.title          || null;
    this.description     = obj && obj.description    || null;
    this.thumbnailUrl    = obj && obj.thumbnailUrl   || null;
    this.videoUrl        = obj && obj.videoUrl       ||
                             `https://www.youtube.com/watch?v=${this.id}`;
  }
}

// Defined YsearchService using Injectable DI
@Injectable()
       export class YsearchService{
       constructor(private http:Http,
           @Inject(Y_API_KEY) private api_key:string,
           @Inject(Y_API_URL) private api_url: string){

             }
        search(query : string):Observable<SearchResult[]>{
            let params : string=[
                `q=${query}`,
                `key=${this.api_key}`,
                `part=snippet`,
                `type=video`,
                `maxResults=30`,
                `videoSyndicated=any`
            ].join('&');
            let queryUrl : string =`${this.api_url}?${params}`;
            return this.http.get(queryUrl)
            .map((response:Response)=>{
                return (<any>response.json()).items.map(item=>{
                    return new SearchResult({
                        id: item.id.videoId,
                        title: item.snippet.title,
                        description: item.snippet.description,
                        thumbnailUrl: item.snippet.thumbnails.high.url,
                        liveBroadcastContent: item.snippet.liveBroadcastContent
                    });
                });
            });
        }
    }
    //Key Injection / defining Injectable providers
    export var yServiceInjectables: Array<any> = [
        {provide: YsearchService, useClass: YsearchService},
        {provide: Y_API_KEY , useValue: Y_API_KEY},
        {provide: Y_API_URL , useValue: Y_API_URL}
    ];

    //SearchBox Component definition

    @Component({
        selector:'search-b',
        outputs:['loading','results'],
        template:`<div class="ui action input">
          <input type="text" placeholder="Search...">
        </div>`
    })

    export class SearchBoxComponent implements OnInit {
      loading: EventEmitter<boolean> = new EventEmitter<boolean>();
      results: EventEmitter<SearchResult[]> = new EventEmitter<SearchResult[]>();

      constructor(private youtube:YsearchService,
                  private el: ElementRef) {
      }

      ngOnInit(): void {
        // convert the `keyup` event into an observable stream
        Observable.fromEvent(this.el.nativeElement, 'keyup')
          .map((e: any) => e.target.value) // extract the value of the input
          .filter((text: string) => text.length > 1) // filter out if empty
          .debounceTime(250)                         // only once every 250ms
          .do(() => this.loading.next(true))         // enable loading
          // search, discarding old events if new input comes in
          .map((query: string) => this.youtube.search(query))
          .switch()
          // act on the return of the search
          .subscribe(
            (results: SearchResult[]) => { // on sucesss
              this.loading.next(false);
              this.results.next(results);
            },
            (err: any) => { // on error
              this.loading.next(false);
            },
            () => { // on completion
              this.loading.next(false);
            }
          );

      }
    }

    //results

    @Component({
        inputs:['result'],
        selector:'search-results',
        template:`
    <div class="ui link cards">
  <div class="card">
    <div class="image">
      <img src="{{result.thumbnailUrl}}">
    </div>
    <div class="content">
      <div class="header">{{result.title}}</div>
      <div class="meta">
        <a href="{{result.videoUrl}}">Watch</a>
      </div>
      <div class="description wrap">
        {{result.description}}
      </div>
    </div>
    <div class="extra content">
      <span class="right floated">
      </span>
      <span>
      <a href="{{result.videoUrl}}">
        <i class="user icon"></i>
        </a>
        <i class="bullseye icon ui orange" *ngIf="result.liveBroadcastContent">Live</i>
      </span>
    </div>
  </div>
  <div>
        `
    })
    export class SearchResultsComponent{
        result : SearchResult;
    }

    //Ysearch Component
    @Component({
        selector : 'y-search',
        templateUrl : './ysearch.component.html',
        styleUrls: []
    })


    export class YsearchComponent{
        results : SearchResult[];
        updateResults(results:SearchResult[]):void{
            this.results = results;
        }
       constructor(){

       }
    }
